require 'nn'
local activationMaximization, parent = torch.class('nnvisual.criterion.activationMaximization', 'nn.Module')

function activationMaximization:__init(mask)
    assert(torch.isTensor(mask), 'the mask should be a tensor')
    parent.__init(self)
    self.mask = mask:clone()
end

function activationMaximization:updateOutput(input)
    self.output = - torch.dot(input, self.mask)
    return self.output
end

function activationMaximization:updateGradInput(input, gradOutput)
    self.gradInput = - self.mask
    return self.gradInput
end
