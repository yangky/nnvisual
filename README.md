nnvisual
==============

A toolkit for visualizing and understanding neural networks.  
(This is a project in preparation for my bachelor thesis)

## Features

* various visualization algorithm  
* multiple backends: images, web pages, etc.
* close integration with the Torch eco-system(torch, nn, dp, etc.).  
* efficient, modular, easy to use and contribute.  
* both off-the-shelf visualizations and ability to develop one's own algorithms.  
* manage multiple experiments and monitor their progress.
* integrate with itorch and ssh, like [python](https://github.com/paramiko/paramiko/) or [C](www.libssh.org) functionality to connect to servers in itorch  

## Visualizaion Methods

(organize them into different sub-package ?)

* activation maximization starting from a random image  
* activation maximization using images in the validation set  
* activation maximization for multiple neurons  
* inversion
* partial inversion
* batch inversion, imposing diversity, etc. 
* caricaturization  
* deconv  
* visualize activations  
* visualize weights  
* visualize feature space: t-SNE  
* image-specific saliency map, i.e. data gradient  
* adversarial examples  
* RNNs, LSTMs
* visualize the architecture of CNNs  
* visualize the training process, with multiple statistics  


## Regularization

### direct 

* Lp norm
* isotropic norm
* hard constraints    
* total variation

### indirect

* jittering
* Gaussian blurring
* clipped pixels

### a combination of them

## Models

* SIFT  
* HOG  
* AlexNet  
* VGG  
* GoogleLenet  
* MSRA  

## Demos

## Applications

* Understanding how CNNs work and developing intuition.  
* reveal the encoded information   
* Use inversion to study wrong prediction, Vondrick et al.
* Use activation maximization to spot bias in training set, google inceptionism. 
 

* visualize activations from a live webcam  


## Documentation

Hosted on [Read the Docs](http://nnvisual.readthedocs.org/en/latest/)
  

## Ideas

* model natural images(learn to regularize)
* train CNNs togather with autoencoders  
* the effect of filter size on the quality of reconstructed images  
*  multiple initialization
* inverse a hierarchy of features

## Modelling Natural Images

* deep boltzmann machine
* deep belief networks
* conv RBM
* what-where autoencoders
* generative adversarial networks
* gaussian mixtures