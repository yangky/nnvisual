require 'optim'

local function visualize(model, inputSize, criterion, init, optimState, regularizer, folder)
    -- check parameters
    assert(torch.isTypeOf(model, 'nn.Module'), 'the model must be an instance of nn.Module')
    assert(torch.isTypeOf(criterion, 'nn.Module'), 'the criterion must be an instance of nn.Module')
    local useCUDA
    if model:getParameters():type() == 'torch.CudaTensor' then
        useCUDA = true
    else
        useCUDA = false
    end    
    local input
    if init then
        if torch.isTensor(init) then
            input = init
        elseif torch.isTypeOf(init, 'torch.LongStorage') then
            input = torch.Tensor():resize(inputSize):uniform(init[1], init[2])
        else
            error('the init must be either a tensor or a torch.LongStorage')
        end
    else
        assert(torch.isTypeOf(inputSize, 'torch.LongStorage'), 'the inputSize must be an instance of torch.LongStorage')
        input = torch.Tensor():resize(inputSize):uniform()
    end
    if useCUDA then
        input = input:cuda()
    end
    assert(type(optimState) == 'table', 'the optimState must be a table')
    local optimAlgo = optim[optimState['algorithm']]
    local maxIter = optimState['maxIter']
    if regularizer ~= nil then
        assert(type(regularizer) == 'function', 'the regularizer must be a function')
    else
        regularizer = function(x) return 0, torch.Tensor():typeAs(x):resizeAs(x):zero() end
    end
    
    -- define loss function
    function loss(x)
        
        local f, grad = regularizer(x)    
        --print(string.format('reg = %f', f)) 
        local output = model:forward(x)
        --print(output); 
        f = f + criterion:forward(output)
        --print(string.format('total loss = %f', f))
        local gradOutput = criterion:backward(output)
        model:backward(x, gradOutput)
        grad:add(model.gradInput)
        --print('gradnorm: ' .. tostring(grad:norm()))
        return f, grad
    end

    -- run optimization
    local trace = {}
    local sum = 0
    local gap = 500
    
    for iter = 1, maxIter do
        xlua.progress(iter, maxIter)
        if iter == math.ceil(0.75 * maxIter) then
            optimState.learningRate = optimState.learningRate / 10
        elseif iter == math.ceil(0.95 * maxIter) then
            optimState.learningRate = optimState.learningRate / 10
        end
        local _, cost = optimAlgo(loss, input, optimState)
        assert(cost[1] == cost[1], 'nan during training')
        --print(cost[1])
        sum = sum + cost[1]
    if folder ~= nil and (iter - 1) % 50  == 0 then
       model:get(1):forward(input)
       image.save(string.format('%s/%d.png', folder, iter), model:get(1):get(16).output:clone():squeeze())
    end
        if iter % gap == 0 then
            local avg = sum / gap
            table.insert(trace, avg)
            --print(avg)
            sum = 0
            if #trace >= 3 and trace[#trace] >= trace[#trace - 1] and trace[#trace - 1] >= trace[#trace - 2] then
                --print(avg)
                --break
            end
        end
    end

    return input
end

return visualize
