require 'torch'
require 'nn'
local RestrictedBoltzmannMachine = torch.class('RestrictedBoltzmannMachine')

function RestrictedBoltzmannMachine:__init(n_visible, n_hidden, k, miniBatch)
    -- check parameters
    assert(type(n_visible) == 'number' and math.floor(n_visible) == n_visible
           and n_visible > 0, 'n_visible must be a integer greater than 0')
    assert(type(n_hidden) == 'number' and math.floor(n_visible) == n_visible
           and n_hidden > 0, 'n_hidden must be a integer greater than 0')
    assert(torch.isTensor(miniBatch), 'miniBatch must be a tensor')
    self.batchsize = miniBatch:size(1)
    
    self.n_visible = n_visible
    self.n_hidden = n_hidden
    self.k = k
    
    self.Tensor = torch.Tensor
    
    self.weight = self.Tensor(n_visible, n_hidden)
    self.gradWeight = self.Tensor():resizeAs(self.weight)
    self.biasVisible = self.Tensor(n_visible)
    self.gradBiasVisible = self.Tensor():resizeAs(self.biasVisible)
    self.biasHidden = self.Tensor(n_hidden)
    self.gradBiasHidden = self.Tensor():resizeAs(self.biasHidden)
    
    self:reset(miniBatch:typeAs(self.weight))
end

function RestrictedBoltzmannMachine:reset(miniBatch)
    -- initialize the weight and bias
    self.weight:normal(0, 0.01)
    self.biasHidden:zero()
    self.biasVisible:zero()
    -- initialize the Markov chains for Persistent Contrastive Divergence(PCD)
    assert(0 <= miniBatch:min() and miniBatch:max() <= 1, 'data must be preprocessed into [0, 1]')
    local h, ph = self:updateHidden(miniBatch)
    self.chains = {
        visible = miniBatch:clone(),
        pv = miniBatch:clone(),
        hidden = h,
        ph = ph,
    }
end

function RestrictedBoltzmannMachine:parameters()
    return {self.weight, self.biasVisible, self.biasHidden}, {self.gradWeight, self.gradBiasVisible, self.gradBiasHidden}
end

function RestrictedBoltzmannMachine:getParameters()
   local parameters,gradParameters = self:parameters()
   return nn.Module.flatten(parameters), nn.Module.flatten(gradParameters)
end

function RestrictedBoltzmannMachine:updateHidden(v)
    assert(torch.isTensor(v) and v:size(1) == self.batchsize
           and v:size(2) == self.n_visible, 'v must be a tensor of size batchsize x n_visible')
    -- v:           batchsize x n_visible
    -- self.weight: n_visible x n_hidden
    -- h:           batchsize x n_hidden
    local ph = torch.addmm(self.biasHidden:view(1, -1):expand(self.batchsize, self.n_hidden), v, self.weight):sigmoid()
    local h = torch.lt(torch.Tensor():resizeAs(ph):uniform(), ph):typeAs(ph)
    return h, ph
end

function RestrictedBoltzmannMachine:updateVisible(h)
    assert(torch.isTensor(h) and h:size(1) == self.batchsize
           and h:size(2) == self.n_hidden, 'h must be a tensor of size batchsize x n_hidden')
    -- h:           batchsize x n_hidden
    -- self.weight: n_visible x n_hidden
    -- v:           batchsize x n_visible
    local pv = torch.addmm(self.biasVisible:view(1, -1):expand(self.batchsize, self.n_visible), h, self.weight:transpose(1, 2)):sigmoid()
    local v = torch.lt(torch.Tensor():resizeAs(pv):uniform(), pv):typeAs(pv)
    return v, pv
end

function RestrictedBoltzmannMachine:updateChains()
    for i = 1, self.k do
        self.chains.hidden, self.chains.ph = self:updateHidden(self.chains.visible)
        self.chains.visible, self.chains.pv = self:updateVisible(self.chains.hidden)
    end
end

function RestrictedBoltzmannMachine:zeroGradParameters()
    self.gradWeight:zero()
    self.gradBiasVisible:zero()
    self.gradBiasHidden:zero()
end

function RestrictedBoltzmannMachine:accGradParameters(v)
    assert(0 <= v:min() and v:max() <= 1, 'data must be preprocessed into [0, 1]')
    -- positive phase
    local h, ph = self:updateHidden(v)
        -- v: batchsize x n_visible x 1
        -- h: batchsize x 1 x n_hidden
        -- weight_pos: n_visible x n_hidden
    local weight_pos = torch.bmm(v:view(self.batchsize, self.n_visible, 1), h:view(self.batchsize, 1, self.n_hidden)):mean(1)
    local biasVisible_pos = v:mean(1)
    local biasHidden_pos = h:mean(1)
    
    -- negative phase
    self:updateChains()
        -- self.chains.pv:       batchsize x n_visible x 1
        -- self.chains.ph:       batchsize x 1 x n_hidden
        -- weight_neg:          n_visible x n_hidden
    local weight_neg = torch.bmm(self.chains.pv:view(self.batchsize, self.n_visible, 1), 
                                 self.chains.ph:view(self.batchsize, 1, self.n_hidden)):mean(1)
    local biasVisible_neg = self.chains.pv:mean(1) 
    local biasHidden_neg = self.chains.ph:mean(1)
    
    -- compute gradient
    self.gradWeight:add(weight_neg):csub(weight_pos)
    self.gradBiasVisible:add(biasVisible_neg):csub(biasVisible_pos)
    self.gradBiasHidden:add(biasHidden_neg):csub(biasHidden_pos)
 
end

function RestrictedBoltzmannMachine:freeEnergy(v)
    assert(torch.isTensor(v) and v:size(2) == self.n_visible, 'v must be a tensor of size batchsize x n_visible')
    local batchsize = v:size(1)
    -- v:           batchsize x n_visible
    -- self.weight: n_visible x n_hidden
    -- h:           batchsize x n_hidden
    local x = torch.addmm(self.biasHidden:view(1, -1):expand(batchsize, self.n_hidden), v, self.weight)
    local f = torch.exp(x):add(1):log():sum(2):sign()
    f:csub(torch.cmul(v, self.biasVisible:view(1, -1):expand(batchsize, self.n_visible)):sum(2))
    f = f:mean()
    local grad = (x:sigmoid() * self.weight:transpose(1, 2)):mean(1):add(self.biasVisible):neg()
    return f, grad
end

function RestrictedBoltzmannMachine:logProbability(v)
    assert(torch.isTensor(v) and v:size(2) == self.n_visible, 'v must be a tensor of size batchsize x n_visible')

end

function RestrictedBoltzmannMachine:sample(burnin, nsamp, gap)
    assert(burnin >= 0, 'burnin must be a non-negative integer')
    assert(nsamp >= 0, 'nsamp must be a non-negative integer')
    assert(gap >= 0, 'gap must be a non-negative integer')
    -- initialization
    local v = self.Tensor(self.batchsize, self.n_visible):uniform()
    local h = self:updateHidden(v)  
    -- burn in 
    for iter = 1, burnin do
        v = self:updateVisible(h)
        h = self:updateHidden(v) 
    end
    -- draw samples
    local samples = {}
    local iter = 0
    while #samples < nsamp do
        v, pv = self:updateVisible(h)
        h, ph = self:updateHidden(v)
        if iter % (gap + 1) == 0 then
            samples[#samples + 1] = {
                visible = v,
                pv = pv,
                hidden = h,
                ph = ph,
            }
        end
        iter = iter + 1
    end
    
    return samples
end

function RestrictedBoltzmannMachine:type(type)
    assert(type, 'RestrictedBoltzmannMachine: must provide a type to convert to')

    if type == 'torch.FloatTensor' then
        self.Tensor = torch.CudaTensor
    elseif tyoe == 'torch.DoubleTensor' then
        self.Tensor = torch.DoubleTensor
    elseif type == 'torch.CudaTensor' then
        self.Tensor = torch.CudaTensor
    else 
        error('no such tensor type: ' .. type)
    end
    
    self.weight = self.weight:type(type)
    self.gradWeight = self.gradWeight:type(type)
    self.biasVisible = self.biasVisible:type(type)
    self.gradBiasVisible = self.gradBiasVisible:type(type)
    self.biasHidden = self.biasHidden:type(type)
    self.gradBiasHidden = self.gradBiasHidden:type(type)

    self.chains.visible = self.chains.visible:type(type)
    self.chains.pv = self.chains.pv:type(type)
    self.chains.hidden = self.chains.hidden:type(type)
    self.chains.ph = self.chains.ph:type(type)

   return self
end

function RestrictedBoltzmannMachine:float()
   return self:type('torch.FloatTensor')
end

function RestrictedBoltzmannMachine:double()
   return self:type('torch.DoubleTensor')
end

function RestrictedBoltzmannMachine:cuda()
   return self:type('torch.CudaTensor')
end

