require 'paths'
local regularization = paths.dofile('regularization.lua')

local function checkGradients(regularizer, inputSize, eps)
    assert(type(regularizer) == 'function', 'the regularizer must be a funciton')
    assert(torch.isTypeOf(inputSize, 'torch.LongStorage'), 'the inputSize must be a torch.LongStorage')
    eps = eps or 1e-4
    
    local function checkgrad(func, x0)
        local n = x0:nElement()
        local f0, aGrad = func(x0)
        local nGrad = torch.Tensor(n)
        for j = 1, n do
            local tmp = torch.zeros(n)
            tmp[j] = eps
            local x2 = x0 + tmp:resizeAs(x0)
            f2 = func(x2)
            local x1 = x0 - tmp:resizeAs(x0)
            f1 = func(x1)
            nGrad[j] = (f2 - f1) / (2 * eps)
        end
        local err = torch.dist(nGrad:resizeAs(aGrad), aGrad) / aGrad:norm()
        --print(torch.cat(torch.cat(aGrad:view(2352, 1), nGrad:view(2352, 1)), torch.abs(aGrad:view(2352, 1) - nGrad:view(2352, 1))))
        assert(err < eps, string.format('relative error exceeds %f: %f', eps, err))
    end
    
    for i = 1, 10 do
        local x0 = torch.Tensor():resize(inputSize):normal()
        checkgrad(regularizer, x0)
    end
end

local inputSize = torch.LongStorage{28, 28}
checkGradients(regularization.weightDecay(2, 1), inputSize)
checkGradients(regularization.weightDecay(3, 1), inputSize)
checkGradients(regularization.weightDecay(51, 1), inputSize)
inputSize = torch.LongStorage{1, 28, 28}
checkGradients(regularization.totalVariation(2, 12, 1), inputSize)
checkGradients(regularization.totalVariation(1, 12, 1), inputSize)
inputSize = torch.LongStorage{3, 12, 12}
--checkGradients(regularization.isotropicWeightDecay(2, 80, 1), inputSize)
--checkGradients(regularization.isotropicWeightDecay(3, 80, 1), inputSize)
checkGradients(regularization.totalVariation(2, 12, 1), inputSize)
checkGradients(regularization.totalVariation(1, 12, 1), inputSize)
checkGradients(regularization.totalVariation(51, 12, 1), inputSize)