require 'torch'
torch.setdefaulttensortype('torch.FloatTensor')
torch.seed()
require 'image'
local nnvisual = paths.dofile('../nnvisual.lua')
--local labels = paths.dofile('cifar10Labels.lua')

local cmd = torch.CmdLine()
cmd:text()
cmd:text('Activation Maximization Visualization for a ConvNet Trained on CIFAR-10 with DCGAN Regularization')
cmd:text('Example:')
cmd:text('$> qlua main.lua --dislay --model /path/to/model')
cmd:text('Options:')
cmd:option('--model',           './net.t7',             'path to the model to be visualized')
cmd:option('--layer',           29,                     'the layer to visualize')
cmd:option('--width',           1,                      'the width of output of that layer')
cmd:option('--height',          1,                      'the height of output of that layer')
cmd:option('--dcgan',           './dcgan_G.t7',         'path to the image generator')
cmd:option('--nz',              100,                    'latent feature dimension')
cmd:option('--optimAlgo',       'rmsprop',              'optimization algorithm')
cmd:option('--maxIter',         10000,                  'the maximum number of iterations')
cmd:option('--learningRate',    5e-3,                   'learning rate')
cmd:option('--momentum',        0.9,                    'momentum')
cmd:option('--weightDecay',     1e-4,                   'weight decay')
cmd:option('--cuda',            false,                  'use CUDA')
cmd:option('--GPU',             1,                      'the GPU to use')
cmd:option('--display',         false,                  'display the results(qlua required)')
cmd:option('--output',          './invDCGAN.png',    'image saved')
cmd:text()

local opts = cmd:parse(arg or {})
print(opts)

local model = torch.load(opts.model)
local size = model:size()
for i = 1, size - opts.layer do
    model:remove()
end
local dcgan_G = torch.load(opts.dcgan)
local concat = nn.Sequential():add(dcgan_G):add(model)
print(concat)
concat:evaluate()
if opts.cuda then
    require 'cutorch'
    cutorch.setDevice(opts.GPU)
    require 'cunn'
    require 'cudnn'
    concat:cuda()
end
concat:forward(torch.Tensor(1, opts.nz, 1, 1):normal():cuda())

local results = {}
local labels = {}

for i = 0, 9 do
    labels[i + 1] = string.format('layer%d_unit%d', opts.layer, i + 1)
    print(string.format('visualizing %s', labels[i + 1]))
    local mask = concat.output:view(-1, opts.width, opts.height):clone():zero()
    mask[{i + 1, math.ceil(opts.height / 2),  math.ceil(opts.width / 2)}] = 1
    local criterion = nnvisual.criterion.activationMaximization(mask)
    if opts.cuda then
        criterion:cuda()
    end
    local state = {
        algorithm = opts.optimAlgo,
        maxIter = opts.maxIter,
        learningRate = opts.learningRate,
        momentum = opts.momentum,
        weightDecay = 0,
    }
    local latent = nnvisual.visualize(concat,
                                      nil,
                                      criterion,
                                      torch.Tensor(1, opts.nz, 1, 1):normal():cuda(),
                                      state,
                                      nil)
    concat:get(1):forward(latent)
    table.insert(results, concat:get(1):get(16).output:clone():squeeze())
    image.save(string.format('%s_%s.png', opts.output, labels[i + 1]), results[#results])
--    torch.save(string.format('%s_%s_latent.t7', opts.output, labels[i + 1]), latent)
end

local panel = image.toDisplayTensor(results)
--image.save(opts.output, panel)
if opts.display then
    image.display(results)
end
