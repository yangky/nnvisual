import random
import os
import math
import sys
import time
import itertools


weightDecays = [-10, -8, -6, -4, -2, 0]
    
for weightDecay in weightDecays:
    cmd = 'th actMaxDCGAN.lua --cuda --weightDecay 10e%d --output weightDecay_1e%d.png' % (weightDecay, weightDecay)
    print(cmd)
    os.system(cmd)
    time.sleep(1)
