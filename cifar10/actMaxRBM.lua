require 'torch'
torch.setdefaulttensortype('torch.FloatTensor')
require 'nn'
require 'nnx'
require 'optim'
require 'image'
local nnvisual = require 'nnvisual'
-- paths.dofile('RestrictedBoltzmannMachine.lua')
-- local rbm = torch.load('rbm-mnist.t7')
local dcgan_G = torch.load('dcgan_G.t7')


local cmd = torch.CmdLine()
cmd:text()
cmd:text('Activation Maximization Visualization for a ConvNet Trained on CIFAR-10 with DCGAN Regularization')
cmd:text('Example:')
cmd:text('$> th main.lua --model /path/to/model')
cmd:text('Options:')
cmd:option('--model',           './net.t7',         'path to the model to be visualized')
cmd:option('--optimAlgo',       'rmsprop',          '')
cmd:option('--maxIter',         1000,               '')
cmd:option('--learningRate',    1e-2,               '')
cmd:option('--momentum',        0.9,                '')
cmd:option('--weightDecay',     1e-6,               '')
cmd:option('--output',          'actMaxDCGAN.png',  '')
cmd:option('--cuda',            false,              '')
cmd:text()

local opts = cmd:parse(arg or {})
opts.width = 32
opts.height = 32
opts.channels = 3
print(opts)

local model = torch.load(opts.model)
model:evaluate()
if opts.cuda then
    require 'cudnn'
    model:cuda()
end

local results = {}

for i = 0, 9 do
    print(string.format('visualizing class %d', i))
    local mask = torch.zeros(10)
    mask[i + 1] = 1
    local criterion = nnvisual.criterion.activationMaximization(mask)
    if opts.cuda then
        criterion:cuda()
    end
    local state = {
        algorithm = opts.optimAlgo,
        maxIter = opts.maxIter,
        learningRate = opts.learningRate,
        momentum = opts.momentum,
        weightDecay = 0,
    }
    local latent = nnvisual.visualize(concat,
                                      torch.LongStorage{1, 100, 1, 1},
                                      criterion,
                                      torch.Tensor(1, 100, 1, 1):normal(),
                                      state,
                                      nil)
    
    table.insert(results, concat:get(1):forward(latent))

end

image.display(panel)
--image.save(opts.output, panel)
--image.save(string.format('%d-%.10f-%d-%.10f-%.10f-%.10f.png', opts.p, opts.weightDecay, opts.jitter, opts.beta, opts.V, opts.TV), panel)
