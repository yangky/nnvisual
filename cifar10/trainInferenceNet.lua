torch.setdefaulttensortype('torch.FloatTensor')
require 'nn'
require 'optim'
require 'cunn'
require 'cudnn'

local cmd = torch.CmdLine()
cmd:option('--GPU', 1, '')
cmd:option('--batchsize', 32, '')
cmd:option('--learningRate', 1e-3, '')
cmd:option('--weightDecay', 0, '')
cmd:option('--maxIter', 200000, '')
local opts = cmd:parse(arg)
cutorch.setDevice(opts.GPU)

local dcgan_G = torch.load('dcgan_G.t7')
dcgan_G:cuda()
dcgan_G = cudnn.convert(dcgan_G, cudnn)

local infNet = nn.Sequential()

local nc = 3
local ndf = 64
local nz = 100

-- input is 3 x 64 x 64
infNet:add(nn.SpatialConvolution(nc, ndf, 4, 4, 2, 2, 1, 1))
infNet:add(nn.LeakyReLU(0.2, true))
-- state size: (ndf) x 32 x 32
infNet:add(nn.SpatialConvolution(ndf, ndf * 2, 4, 4, 2, 2, 1, 1))
infNet:add(nn.SpatialBatchNormalization(ndf * 2)):add(nn.LeakyReLU(0.2, true))
-- state size: (ndf*2) x 16 x 16
infNet:add(nn.SpatialConvolution(ndf * 2, ndf * 4, 4, 4, 2, 2, 1, 1))
infNet:add(nn.SpatialBatchNormalization(ndf * 4)):add(nn.LeakyReLU(0.2, true))
-- state size: (ndf*4) x 8 x 8
infNet:add(nn.SpatialConvolution(ndf * 4, ndf * 8, 4, 4, 2, 2, 1, 1))
infNet:add(nn.SpatialBatchNormalization(ndf * 8)):add(nn.LeakyReLU(0.2, true))
-- state size: (ndf*8) x 4 x 4
infNet:add(nn.SpatialConvolution(ndf * 8, nz, 4, 4))
-- state size: 100 x 1 x 1
infNet:add(nn.View(1):setNumInputDims(3))
-- state size: 100


local criterion = nn.MSECriterion()
infNet:cuda()
infNet = cudnn.convert(infNet, cudnn)
criterion:cuda()

local param, gradparam = infNet:getParameters()

local state = {
   learningRate = opts.learningRate,
   weightDecay = opts.weightDecay,
}
local z = torch.Tensor(opts.batchsize, nz, 1, 1):cuda()

local function loss(x)
    z:normal()
    dcgan_G:forward(z)
    local input = dcgan_G:get(16).output
    infNet:zeroGradParameters()
    local output = infNet:forward(input)
    local f = criterion:forward(output, z)
    infNet:backward(input, criterion:backward(output, z))
    return f, gradparam
end

local avgCost = 0
local gap = 1000
for iter = 1, opts.maxIter do
    if iter == 0.75 * opts.maxIter or iter == 0.95 * opts.maxIter then
        state.learningRate = state.learningRate / 10
    end
    local _, cost = optim.rmsprop(loss, param, state)
    assert(cost[1] == cost[1], 'nan during training')
    avgCost = avgCost + cost[1]
    --print(cost[1])
    if iter % gap == 0 then
        avgCost = avgCost / gap
        print(avgCost)
        avgCost = 0
        infNet:clearState()
        local model = infNet:clone()
        model:float()
        torch.save('infNet.t7', model)
    end
--    xlua.progress(iter, opts.maxIter)
end
