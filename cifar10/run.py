import os
import time
import sys

layer = int(sys.argv[1])
GPU = int(sys.argv[2])

labels = ['airplane', 'automobile', 'bird', 'cat', 'deer', 'dog', 'frog', 'horse', 'ship', 'truck']

for i in xrange(10):
  for j in xrange(5):
    cmd = 'th invDCGAN.lua --cuda --layer %d --src %s_%d.png --GPU %d' % (layer, labels[i], j + 1, GPU)
    print(cmd)
    os.system(cmd)
    time.sleep(1)
