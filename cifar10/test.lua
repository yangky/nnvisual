torch.setdefaulttensortype('torch.FloatTensor')
require 'dp'
require 'cunn'
require 'cudnn'

local ds = dp.Cifar10()
local X = ds:loadTest():inputs():input():cuda()
local y = ds:loadTest():targets():input():cuda()
local model = cudnn.convert(torch.load('net.t7'), cudnn)
model:cuda()

for i = 1, 9999, 100 do
    local X_batch = X:narrow(1, i, 100)
    local y_batch = y:narrow(1, i, 100)
    local output = model:forward(X_batch)
    for j = 1, 100 do
        print(string.format('%d %f', i + (j - 1), output[j][y_batch[j]]))
    end
end
