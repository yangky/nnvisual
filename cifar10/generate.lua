require 'image'
require 'nnx'
torch.setdefaulttensortype('torch.FloatTensor')

local cmd = torch.CmdLine()
cmd:text()
cmd:text('Generate Images with a DCGAN Generator')
cmd:text('Example:')
cmd:text('$> th generate.lua')
cmd:text('Options:')
cmd:option('--batchSize',   36,                'number of samples to produce')
cmd:option('--noisetype',   'normal',           'type of noise distribution (uniform / normal)')
cmd:option('--net',         './dcgan_G.t7',     'path to the generator network')
cmd:option('--name',        './generated.png',  'the file saved')
cmd:option('--cuda',        false,              'use cuda accerleration')
cmd:option('--nz',          100,                'lantent space dimension')

local opts = cmd:parse(arg or {})
print(opts)

local noise = torch.Tensor(opts.batchSize, opts.nz, 1, 1)
if opts.noisetype == 'uniform' then
    noise:uniform(-1, 1)
elseif opts.noisetype == 'normal' then
    noise:normal(0, 1)
end


local net = torch.load(opts.net)
print(net)
if opts.cuda then
    require 'cudnn'
    net:cuda()
    cudnn.convert(net, cudnn)
    noise = noise:cuda()
else
   net:float()
end

local images = net:forward(noise)
image.save(opts.name, image.toDisplayTensor(images))
print('saved to: ', opts.name)

