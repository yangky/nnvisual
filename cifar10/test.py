import os
from glob import glob
from scipy.misc import imread, imsave
import numpy as np

for layer in [1, 5, 9, 14, 18, 23]:
  os.system('mkdir layer%d' % layer)
  for unit in xrange(1, 11):
    os.system('mkdir layer%d/unit%d' % (layer, unit))
    large = np.zeros(shape=(8*64, 8*64, 3))
    for n in xrange(64):
      i = n / 8
      j = n % 8
      filename = '%04d.png_layer%d_unit%d.png' % (n, layer, unit)
      large[i*64:(i+1)*64, j*64:(j+1)*64] = imread(filename)
      os.system('cp %s layer%d/unit%d/' % (filename, layer, unit))
    imsave('layer%d_unit%d.png' % (layer, unit), large)
