require 'torch'
torch.setdefaulttensortype('torch.FloatTensor')
require 'image'
local nnvisual = paths.dofile('../nnvisual.lua')
local labels = paths.dofile('cifar10Labels.lua')

local cmd = torch.CmdLine()
cmd:text()
cmd:text('Activation Maximization Visualization for a ConvNet Trained on CIFAR-10')
cmd:text('Example:')
cmd:text('$> qlua main.lua --dislay --model /path/to/model')
cmd:text('Options:')
cmd:option('--model',           './net.t7',         '')
cmd:option('--optimAlgo',       'rmsprop',          '')
cmd:option('--maxIter',         1000,               '')
cmd:option('--learningRate',    1e-2,               '')
cmd:option('--momentum',        0.9,                '')
cmd:option('--p',               2,                  '')
cmd:option('--weightDecay',     1e-5,               '')
cmd:option('--beta',            1,                  '')
cmd:option('--V',               10,                 '')
cmd:option('--TV',              0.1,                '')
cmd:option('--output',          'actMaxAdhoc.png',  '')
cmd:option('--cuda',            false,              '')
cmd:option('--display',         false,              '')
cmd:text()

local opts = cmd:parse(arg or {})
opts.width = 32
opts.height = 32
opts.channels = 3
print(opts)

local model = torch.load(opts.model)
model:evaluate()
print(model)
if opts.cuda then
    require 'cudnn'
    model:cuda()
end

local results = {}

for i = 0, 9 do
    print(string.format('visualizing %s', labels[i + 1]))
    local mask = torch.zeros(10)
    mask[i + 1] = 1
    local criterion = nnvisual.criterion.activationMaximization(mask)
    if opts.cuda then
        criterion:cuda()
    end  
    local state = {
        algorithm = opts.optimAlgo,
        maxIter = opts.maxIter,
        learningRate = opts.learningRate,
        momentum = opts.momentum,
        weightDecay = 0,
    }
    local weightDecay = nnvisual.regularization.weightDecay(opts.p, opts.weightDecay)
    local hardConstraint = nnvisual.regularization.hardConstraint(0, 1)
    local totalVariation = nnvisual.regularization.totalVariation(opts.beta, opts.V, opts.TV)
    local regularizer = nnvisual.regularization.combination({hardConstraint, weightDecay, totalVariation})
    local res = nnvisual.visualize(model, torch.LongStorage{1, 3, opts.height, opts.width}, criterion, nil, state, regularizer)
    table.insert(results, res:squeeze())
    break
end

local panel = image.toDisplayTensor(results)
image.save(opts.output, panel)
if opts.display then
    image.display(results)
end