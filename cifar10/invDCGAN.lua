require 'torch'
torch.setdefaulttensortype('torch.FloatTensor')
torch.seed()
require 'image'
local nnvisual = paths.dofile('../nnvisual.lua')
--local labels = paths.dofile('cifar10Labels.lua')

local cmd = torch.CmdLine()
cmd:text()
cmd:text('Inversion Visualization for a ConvNet Trained on CIFAR-10 with DCGAN Regularization')
cmd:text('Example:')
cmd:text('$> qlua main.lua --dislay --model /path/to/model')
cmd:text('Options:')
cmd:option('--model',           './net.t7',             'path to the model to be visualized')
cmd:option('--layer',           29,                     'the layer to visualize')
cmd:option('--width',           1,                      'the width of output of that layer')
cmd:option('--height',          1,                      'the height of output of that layer')
cmd:option('--src',             'ship_1.png',           'path to the image to be inverted' )
cmd:option('--dcgan',           './dcgan_G.t7',         'path to the image generator')
cmd:option('--nz',              100,                    'latent feature dimension')
cmd:option('--optimAlgo',       'rmsprop',              'optimization algorithm')
cmd:option('--maxIter',         10000,                  'the maximum number of iterations')
cmd:option('--learningRate',    1e-2,                   'learning rate')
cmd:option('--momentum',        0.9,                    'momentum')
cmd:option('--weightDecay',     1e-4,                   'weight decay')
cmd:option('--cuda',            false,                  'use CUDA')
cmd:option('--GPU',             1,                      'the GPU to use')
cmd:option('--display',         false,                  'display the results(qlua required)')
cmd:text()

local opts = cmd:parse(arg or {})
print(opts)

local model = torch.load(opts.model)
local size = model:size()
for i = 1, size - opts.layer do
    model:remove()
end
local dcgan_G = torch.load(opts.dcgan)
local concat = nn.Sequential():add(dcgan_G):add(model)
print(concat)
concat:evaluate()
if opts.cuda then
    require 'cutorch'
    cutorch.setDevice(opts.GPU)
    require 'cunn'
    require 'cudnn'
    concat:cuda()
end
concat:forward(torch.Tensor(1, opts.nz, 1, 1):normal():cuda())

local img = image.load(opts.src):view(1, 3, 32, 32)
if opts.cuda then
    img = img:cuda()
end

local criterion = nnvisual.criterion.inversion(model, img)
if opts.cuda then
    criterion:cuda()
end
    
local state = {
    algorithm = opts.optimAlgo,
    maxIter = opts.maxIter,
    learningRate = opts.learningRate,
    momentum = opts.momentum,
    weightDecay = 0,
}

local latent = nnvisual.visualize(concat,
                                  nil,
                                  criterion,
                                  torch.Tensor(1, opts.nz, 1, 1):normal():cuda(),
                                  state,
                                  nil)

concat:get(1):forward(latent)
local result = torch.Tensor(3, 64, 128)
local reference = image.scale(img:float()[1], 64, 64)
result:narrow(3, 1, 64):copy(reference)
result:narrow(3, 65, 64):copy(concat:get(1):get(16).output:clone():squeeze())
image.save(string.format('invDCGAN_layer%d_', opts.layer) .. opts.src, result)
if opts.display then
    image.display(result)
end
