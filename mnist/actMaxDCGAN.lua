require 'torch'
torch.setdefaulttensortype('torch.FloatTensor')
torch.seed()
require 'nnx'
require 'image'
local nnvisual = paths.dofile('../nnvisual.lua')

local cmd = torch.CmdLine()
cmd:text()
cmd:text('Activation Maximization Visualization for a ConvNet Trained on MNIST with DCGAN Regularization')
cmd:text('Example:')
cmd:text('$> qlua main.lua --dislay --model /path/to/model')
cmd:text('Options:')
cmd:option('--model',           './net.t7',             'path to the model to be visualized')
cmd:option('--dcgan',           './dcgan_G.t7',         'path to the image generator')
cmd:option('--nz',              100,                    'latent feature dimension')
cmd:option('--optimAlgo',       'rmsprop',              'optimization algorithm')
cmd:option('--maxIter',         10000,                  'the maximum number of iterations')
cmd:option('--learningRate',    5e-3,                   'learning rate')
cmd:option('--momentum',        0.9,                    'momentum')
cmd:option('--weightDecay',     1e-4,                   'weight decay')
cmd:option('--cuda',            false,                  'use CUDA')
cmd:option('--GPU',             1,                      'the GPU to use')
cmd:option('--display',         false,                  'display the results(qlua required)')
cmd:option('--output',          './actMaxDCGAN.png',    'image saved')
cmd:text()

local opts = cmd:parse(arg or {})
print(opts)

local model = torch.load(opts.model)
local dcgan_G = torch.load(opts.dcgan)
local concat = nn.Sequential():add(dcgan_G):add(model)
print(concat)
if opts.cuda then
    require 'cutorch'
    cutorch.setDevice(opts.GPU)
    require 'cunn'
    require 'cudnn'
    concat:cuda()
end

local results = {}

for i = 0, 9 do
    print(string.format('visualizing digit %d', i))
    local mask = torch.zeros(10)
    mask[i + 1] = 1
    local criterion = nnvisual.criterion.activationMaximization(mask)
    if opts.cuda then
        criterion:cuda()
    end
    local state = {
        algorithm = opts.optimAlgo,
        maxIter = opts.maxIter,
        learningRate = opts.learningRate,
        momentum = opts.momentum,
        weightDecay = 0,
    }
    local latent = nnvisual.visualize(concat,
                                      nil,
                                      criterion,
                                      torch.Tensor(1, opts.nz, 1, 1):normal(),
                                      state,
                                      nil)
    concat:get(1):forward(latent)
    table.insert(results, concat:get(1):get(16).output:clone():squeeze())
    image.save(string.format('%s_%d.png', opts.output, i), results[#results])
    torch.save(string.format('%s_%d_latent.t7', opts.output, i), latent)
end

local panel = image.toDisplayTensor(results)
image.save(opts.output, panel)
if opts.display then
    image.display(results)
end
