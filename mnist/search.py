import random
import os
import math
import sys
import time
import itertools

ps = [6]
weightDecays = [-8, -6, -4, -2, 0]
jitters = [0]
betas = [1, 2, 4]
Vs = [1, 2, 4, 8, 16, 32, 64]
TVs = [0, 0.001, 0.01, 0.1, 1, 10]
    
for (p, weightDecay, jitter, beta, V, TV) in itertools.product(ps, weightDecays, jitters, betas, Vs, TVs):
    cmd = 'th main.lua --p %d --weightDecay 10e%d --jitter %d --beta %d --V %d --TV %.5f' % (p, weightDecay, jitter, beta, V, TV)
    print(cmd)
    os.system(cmd)
    time.sleep(1)