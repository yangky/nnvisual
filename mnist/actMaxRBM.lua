require 'torch'
torch.setdefaulttensortype('torch.FloatTensor')
require 'nn'
require 'optim'
require 'image'
local nnvisual = paths.dofile('../nnvisual.lua')
paths.dofile('../RestrictedBoltzmannMachine.lua')
local rbm = torch.load('rbm-mnist.t7')

local cmd = torch.CmdLine()
cmd:text()
cmd:text('Visualize Trained CNNs')
cmd:text('Example:')
cmd:text('$> th main.lua --type activation-maximization --model /path/to/model')
cmd:text('Options:')
cmd:option('--type',            'activation-maximization',      'type of visualization')
cmd:option('--model',           './net.t7',       'path to the model to be visualized')
cmd:option('--optimAlgo',       'rmsprop',                      '')
cmd:option('--maxIter',         500,                      '')
cmd:option('--learningRate',    1e-2,                         '')
cmd:option('--momentum',        0.9,                           '')
cmd:option('--width',           28,                             '')
cmd:option('--height',          28,                             '')
cmd:option('--p',               6,                           '')
cmd:option('--weightDecay',     1e-6,                           '')
cmd:option('--jitter',           1,                             '')
cmd:option('--beta',            2,                             '')
cmd:option('--V',               12,                             '')
cmd:option('--TV',              0.1,                             '')
cmd:option('--lambda',          2e-4,                       'regularization parameter for rbm')
cmd:option('--output',          'haha.png', '')
cmd:option('--cuda', false, '')

cmd:text()
local opts = cmd:parse(arg or {})


--print('Model: Lenet trained on MNIST')
local model = torch.load(opts.model)
model:evaluate()
if opts.cuda then
    require 'cutorch'
    require 'cunn'
    require 'cudnn'
    model:cuda()
end
if opts.type == 'activation-maximization' then
    --print('Visualization: activation maximization')
    local panel = torch.Tensor(1, 400, 1000)
    for i = 0, 9 do
        --print(string.format('visualizing number %d', i))
        local mask = torch.zeros(10)
        mask[i + 1] = 1
        local criterion = nnvisual.criterion.activationMaximization(mask)
        if opts.cuda then
            criterion:cuda()
        end
        
        local state = {
            algorithm = opts.optimAlgo,
            maxIter = opts.maxIter,
            learningRate = opts.learningRate,
            momentum = opts.momentum,
            weightDecay = 0,
        }
        local weightDecay = nnvisual.regularization.weightDecay(opts.p, opts.weightDecay)
        local hardConstraint = nnvisual.regularization.hardConstraint(0, 1)
        local totalVariation = nnvisual.regularization.totalVariation(opts.beta, opts.V, opts.TV)
        local rbmRegularizer = function(x)
            assert(x:nElement() == 784)
            local f, grad = rbm:freeEnergy(x:view(1, -1))
            return opts.lambda * f, grad:mul(opts.lambda)
        end
        local regularizer = nnvisual.regularization.combination({hardConstraint, weightDecay, rbmRegularizer})
 
        local result = nnvisual.visualize(model, torch.LongStorage{1, 1, opts.height, opts.width}, criterion, nil, state, regularizer)
        
        local row = 1 + math.floor(i / 5)
        local col = 1 + i % 5
        panel:sub(1, -1, 200 * row - 199, 200 * row, 200 * col - 199, 200 * col):copy(image.scale(result:float():squeeze(), 200, 200)) 
    
        break
    end
    
    --image.display(panel)
    image.save(opts.output, panel)
    --image.save(string.format('%d-%.10f-%d-%.10f-%.10f-%.10f.png', opts.p, opts.weightDecay, opts.jitter, opts.beta, opts.V, opts.TV), panel)
    
elseif opts.type == 'inversion' then 
    print('Visualization: inversion')
    local panel = torch.Tensor(1, 1000, 800)
    
    for i = 1, 10 do
        local img = image.load(string.format('./images/%02d.png', i))
        local criterion = nnvisual.inversion(model, img)
        
        local state = {
            algorithm = opts.optimAlgo,
            learningRate = opts.learningRate,
            momentum = opts.momentum,
            weightDecay = 0,
        }
        local weightDecay = nnvisual.regularization.weightDecay(opts.p, opts.weightDecay)
        local hardConstraint = nnvisual.regularization.hardConstraint(0, 1)
        local totalVariation = nnvisual.regularization.totalVariation(opts.beta, opts.V, opts.TV)
        local jitter = nnvisual.regularization.jitter(opts.jitter)
        --local regularizer = nnvisual.regularization.combination({weightDecay, totalVariation, jitter, hardConstraint})

        --local result = nnvisual.visualize(model, torch.LongStorage{1, opts.height, opts.width}, criterion, nil, state, regularizer)
        local result = img:clone()
    
        local row = 1 + math.floor((i - 1) / 2)
        local col = 1 + (i - 1) % 2
        panel:sub(1, -1, 200 * row - 199, 200 * row, 400 * col - 399, 400 * col - 200):copy(image.scale(img, 200, 200)) 
        panel:sub(1, -1, 200 * row - 199, 200 * row, 400 * col - 199, 400 * col):copy(image.scale(result, 200, 200)) 
        
        --break
    end
    
    image.display(panel)
    
else 
    error('unsupported visualization: ' .. opts.type)
end

