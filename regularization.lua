require 'nn'

local regularization = {}

regularization.weightDecay = function(p, lambda)
    assert(type(p) == 'number' and p > 0, 'p must be a positive number')
    if p > 20 then
        print('Warning: weightDecay with large p may have numerical issues.')
    end
    return function(x) 
        local v = math.pow(torch.norm(x, p), p) * lambda
        local grad = torch.pow(x, p - 1) * p * lambda
        if p % 2 == 1 then
            grad:cmul(torch.sign(x))
        end
        assert(v == v, 'nan')
        return v, grad
    end
end

regularization.isotropicWeightDecay = function(p, B, lambda)
    error('gradient check fails.')
    assert(type(p) == 'number' and p > 0, 'p must be a positive number')
    return function(x)
        assert(x:nDimension() == 3, 'the image must be 3-dimensional (channel, height, width)')
        local H = x:size(2)
        local W = x:size(3)
        local tmp = torch.sum(torch.pow(x, 2), 1)
        local v = math.pow(torch.norm(tmp, p / 2), p / 2) 
        v = v * lambda / (H * W * math.pow(B, p))   
        local grad = torch.pow(tmp, p / 2 - 1):view(1, H, W):expandAs(x):cmul(x)
        grad:mul(p * lambda / (H * W * math.pow(B, p)))
        assert(v == v, 'nan')
        return v, grad
    end
end

regularization.hardConstraint = function(a, b)
    assert(a <= b, 'lower bound a should not exceed upper bound b')
    return function(x)
        x:clamp(a, b)
        return 0, torch.Tensor():typeAs(x):resizeAs(x):zero()
    end
end

regularization.totalVariation = function(beta, V, lambda)
    return function(x)
        assert(x:nDimension() == 3 or (x:nDimension() == 4 and x:size(1) == 1), 
               'image must be 3-dimensional (channel, height, width) or 4-dimensional (1, channel, height, width)')
        if x:nDimension() == 4 then
            x = x[1]
        end
        local C = x:size(1)
        local H = x:size(2)
        local W = x:size(3)
     
        local m = nn.SpatialReplicationPadding(1, 1, 1, 1)
        if x:type() == 'torch.CudaTensor' then
            m:cuda()
        end 
        local padX = m:forward(x)
        local x_v_up1 = padX:sub(1, -1, 2, H + 1, 3, W + 2)
        local x_vp1_u = padX:sub(1, -1, 3, H + 2, 2, W + 1)        
        local x_vm1_up1 = padX:sub(1, -1, 1, H, 3, W + 2)
        local x_vm1_u = padX:sub(1, -1, 1, H, 2, W + 1)
        local x_v_um1 = padX:sub(1, -1, 2, H + 1, 1, W)
        local x_vp1_um1 = padX:sub(1, -1, 3, H + 2, 1, W)

        local dx2 =  torch.pow(x_v_up1 - x, 2) + torch.pow(x_vp1_u - x, 2)
        local v = math.pow(torch.norm(dx2, beta / 2), beta / 2)
        v =  lambda * v / (H * W * math.pow(V, beta))

        local eps = 1e-10
        if beta >= 2 then
            eps = 0
        end
        local grad = torch.pow(torch.pow(x_vm1_up1 - x_vm1_u, 2) + torch.pow(x - x_vm1_u, 2) + eps, beta / 2 - 1):cmul(x - x_vm1_u)     
        grad:add(torch.pow(torch.pow(x - x_v_um1, 2) + torch.pow(x_vp1_um1 - x_v_um1, 2) + eps, beta / 2 - 1):cmul(x - x_v_um1))
        grad:csub(torch.pow(dx2 + eps, beta / 2 - 1):cmul(x_v_up1 + x_vp1_u - x * 2))
        grad:mul(lambda * beta / (H * W * math.pow(V, beta)))
        
        assert(v == v, 'nan')
        return v, grad
    end
end

regularization.jitter = function(tau)
    return function(x)
        assert(x:nDimension() == 3 or (x:nDimension() == 4 and x:size(1) == 1), 
               'image must be 3-dimensional (channel, height, width) or 4-dimensional (1, channel, height, width)')
        if x:nDimension() == 4 then
            x = x[1]
        end
        local H = x:size(2)
        local W = x:size(3)  
        local m = nn.SpatialReplicationPadding(tau, tau, tau, tau)
        if x:type() == 'torch.CudaTensor' then
            m:cuda()
        end
        local padX = m:forward(x)
        local tH = math.floor(torch.uniform(-tau, tau) + 0.5)
        local tW = math.floor(torch.uniform(-tau, tau) + 0.5)
        x:copy(padX:sub(1, -1, 1 + tau + tH, tau + tH + H, 1 + tau + tW, tau + tW + W))
        return 0, torch.Tensor():typeAs(x):resizeAs(x):zero()
    end
end

regularization.combination = function(regularizers)
    assert(type(regularizers) == 'table', 'the regularizers must be a list of regularizers')
    return function(x)
        local f = 0
        local grad = torch.Tensor():typeAs(x):resizeAs(x):zero()
        for i = 1, #regularizers do
            local f_i, grad_i = regularizers[i](x)
            f = f + f_i
            grad:add(grad_i)
        end
        assert(f == f, 'nan')
        return f, grad
    end
end


return regularization
