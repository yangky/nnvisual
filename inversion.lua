require 'nn'
local inversion, parent = torch.class('nnvisual.criterion.inversion', 'nn.Module')

function inversion:__init(model, image, mask)
    parent.__init(self)
    local feature = model:forward(image)
    self.mask = mask or torch.Tensor():typeAs(feature):resizeAs(feature):fill(1)
    self.maskedFeature = feature:clone()
    self.maskedFeature:maskedFill(torch.ne(self.mask, 1), 0)
end

function inversion:updateOutput(input)
    local maskedInput = input:clone()
    maskedInput:maskedFill(torch.ne(self.mask, 1), 0)
    self.output = math.pow(torch.dist(maskedInput, self.maskedFeature), 2)
    return self.output
end

function inversion:updateGradInput(input, gradOutput)
    local maskedInput = input:clone()
    maskedInput:maskedFill(torch.ne(self.mask, 1), 0)
    self.gradInput = maskedInput:csub(self.maskedFeature):mul(2)
    return self.gradInput
end
