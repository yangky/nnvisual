require 'torch'
require 'paths'

nnvisual = {}

-- objective functions module
nnvisual.criterion = {} 
paths.dofile('criterion.lua')

-- regularization module
nnvisual.regularization = paths.dofile('regularization.lua')

-- optimization module
nnvisual.visualize = paths.dofile('visualize.lua')

return nnvisual
